package com.gp.training.web.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gp.training.bizlogic.api.model.CityDTO;
import com.gp.training.bizlogic.api.model.CountryDTO;
import com.gp.training.bizlogic.client.resource.DictionaryService;
import com.gp.training.web.shared.model.CityProxy;
import com.gp.training.web.shared.model.CountryProxy;
import com.gp.training.web.shared.service.DictionaryResource;

@Service
public class DictionaryResourceImpl implements DictionaryResource {
	
	@Autowired
	private DictionaryService dictionaryService;
	
	@Override
	public List<CountryProxy> getCountries() {
		List<CountryDTO> dtos = dictionaryService.getCountries();
		
		List<CountryProxy> proxies = new ArrayList<CountryProxy>(dtos.size());
		
		for (CountryDTO dto : dtos) {
			CountryProxy proxy = new CountryProxy();
			
			proxy.setId(dto.getId().intValue());
			proxy.setName(dto.getName());
			proxy.setCode(dto.getCode());
			
			proxies.add(proxy);
		}
		
		return proxies;
	}

	@Override
	public List<CityProxy> getCities() {
		
		List<CityDTO> dtos = dictionaryService.getCities();	
		List<CityProxy> proxies = new ArrayList<CityProxy>(dtos.size());
		
		for (CityDTO dto : dtos) {
			CityProxy proxy = new CityProxy();
			proxy.setId(dto.getId().intValue());
			proxy.setCode(dto.getCode());
			proxy.setName(dto.getName());
			
			if (dto.getCountry() != null) {
				CountryDTO countryDTO = dto.getCountry();
				CountryProxy countryProxy = new CountryProxy();
				
				countryProxy.setId(countryDTO.getId().intValue());
				countryProxy.setName(countryDTO.getName());
				countryProxy.setCode(countryDTO.getCode());
				
				proxy.setCountry(countryProxy);
			}
			proxies.add(proxy);
		}
		
		return proxies;	
	}

}
