package com.gp.training.web.client.ui.booking;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class BookingView extends Composite {

	private static BookingViewUiBinder uiBinder = GWT.create(BookingViewUiBinder.class);

	interface BookingViewUiBinder extends UiBinder<Widget, BookingView> {
	}

	public BookingView() {
		initWidget(uiBinder.createAndBindUi(this));
	}

}
