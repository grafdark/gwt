package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.Booking;

public interface BookingDao {
	
	public Booking create(Booking booking);
}
