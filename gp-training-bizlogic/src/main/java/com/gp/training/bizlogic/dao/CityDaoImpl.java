package com.gp.training.bizlogic.dao;

import org.springframework.stereotype.Repository;

import com.gp.training.bizlogic.domain.City;

@Repository
public class CityDaoImpl extends GenericDaoImpl<City, Long> implements CityDao {

	public CityDaoImpl(){
		super(City.class);
	}
			
}
