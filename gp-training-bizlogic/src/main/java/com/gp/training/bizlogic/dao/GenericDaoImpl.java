package com.gp.training.bizlogic.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public abstract class GenericDaoImpl<T, PK> implements GenericDao<T, PK> {
	
    public static final int MAX_RESULTS = 100;
	
    protected final Class<T> persistentClass;
    protected EntityManager em;
    
    @PersistenceContext(unitName="TRAINING_PERSISTENCE_UNIT")
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    public EntityManager getEntityManager() {
        return this.em;
    }
    
    protected GenericDaoImpl(final Class<T> persistentClass) {
        this.persistentClass = persistentClass;

    }
    
    public class CriteriaSet {
        public final CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        public final CriteriaQuery<T> cr = cb.createQuery(persistentClass);
        public final Root<T> r = cr.from(persistentClass);
    }
    
    
    @Override
    public T get(PK id) {
        T entity = em.find(this.persistentClass, id);
        return entity;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<T> getAll() {
        CriteriaSet cr = new CriteriaSet();
        
        Query query = getEntityManager().createQuery(cr.cr.orderBy(cr.cb.asc(cr.r.get("id"))));
        return query
                .setMaxResults(MAX_RESULTS)
                .getResultList();
    }
}
