package com.gp.training.bizlogic.params;

import java.util.Date;

public class AvailSearchParams {
	
	private long cityId;
	private int guestCount;
	private Date startDate;
	private Date endDate;
	
	public static Builder builder() {
		return new Builder();
	}
	
	public static class Builder {
		private long cityId;
		private int guestCount;
		private Date startDate;
		private Date endDate;
		
		public Builder cityId(long cityId) {
			this.cityId = cityId;
			return this;
		}
		
		public Builder guestCount(int guestCount) {
			this.guestCount = guestCount;
			return this;
		}
		
		public Builder startDate(Date startDate) {
			this.startDate = startDate;
			return this;
		}
		
		public Builder endDate(Date endDate) {
			this.endDate = endDate;
			return this;
		}
		
		public AvailSearchParams build() {
			AvailSearchParams params = new AvailSearchParams();
			params.cityId = cityId;
			params.guestCount = guestCount;
			params.startDate = startDate;
			params.endDate = endDate;
			return params;
		}
	}

	public long getCityId() {
		return cityId;
	}

	public int getGuestCount() {
		return guestCount;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}
	
}
