package com.gp.training.bizlogic.client.resource;

import java.util.List;

import com.gp.training.bizlogic.api.model.CityDTO;
import com.gp.training.bizlogic.api.model.CountryDTO;

public interface DictionaryService {
	
	public List<CountryDTO> getCountries();
	
	public List<CityDTO> getCities();
}
